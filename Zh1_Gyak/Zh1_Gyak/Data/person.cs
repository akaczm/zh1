namespace Zh1_Gyak.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class person
    {
        public int personId { get; set; }

        [StringLength(200)]
        public string personName { get; set; }

        [StringLength(200)]
        public string personPhone { get; set; }

        public int? personFloor { get; set; }
    }
}
