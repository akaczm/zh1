namespace Zh1_Gyak.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PeopleDataContext : DbContext
    {
        public PeopleDataContext()
            : base("name=PeopleDataContext")
        {
        }

        public virtual DbSet<person> people { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<person>()
                .Property(e => e.personName)
                .IsUnicode(false);

            modelBuilder.Entity<person>()
                .Property(e => e.personPhone)
                .IsUnicode(false);
        }
    }
}
