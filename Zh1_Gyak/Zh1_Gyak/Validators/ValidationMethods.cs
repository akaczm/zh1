﻿namespace Zh1_Gyak.Validators
{
    public static class ValidationMethods
    {
        public static bool NotNull(object value)
        {
            return value != null;
        }

        public static bool NotEmpty(string value)
        {
            return value != "";
        }
    }
}
