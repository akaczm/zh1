﻿using System;
using Zh1_Gyak.Attributes;

namespace Zh1_Gyak.Validators
{
    public static class Validator
    {
        public static void Validate(object obj)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                foreach (var attr in prop.CustomAttributes)
                {
                    if (attr.AttributeType == typeof(NotNullAttribute))
                    {
                        if (!ValidationMethods.NotNull(prop.GetValue(obj)))
                        {
                            throw new ArgumentException(string.Format("{0} Property értéke null.", prop.Name));
                        }
                    }
                    else if (prop.PropertyType == typeof(string) && attr.AttributeType == typeof(NotEmptyAttribute))
                    {
                        if (!ValidationMethods.NotEmpty((string)prop.GetValue(obj)))
                        {
                            throw new ArgumentException(string.Format("{0} Property értéke üres.", prop.Name));
                        }
                    }
                }
            }
        }
    }
}
