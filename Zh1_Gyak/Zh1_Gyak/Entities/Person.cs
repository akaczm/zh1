﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zh1_Gyak.Attributes;

namespace Zh1_Gyak.Entities
{
    class Person
    {
        public Person()
        {
        }

        [DbPropertyName("personId")]
        public int ID { get; set; }

        [DbPropertyName("personName")]
        [NotNull]
        [NotEmpty]
        public string Name { get; set; }

        [NotNull]
        [NotEmpty]
        public string Email { get; set; }

        [NotNull]
        [NotEmpty]
        public string Dept { get; set; }

        [NotNull]
        [NotEmpty]
        public string Rank { get; set; }

        [DbPropertyName("personPhone")]
        [NotNull]
        [NotEmpty]
        public string Phone { get; set; }

        [DbPropertyName("personFloor")]
        [NotEmpty]
        public int Room { get; set; }
    }
}
