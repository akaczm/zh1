﻿using System;

namespace Zh1_Gyak.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class DbPropertyNameAttribute : Attribute
    {
        public DbPropertyNameAttribute(string columnName)
        {
            ColumnName = columnName;
        }

        public string ColumnName { get; }
    }
}
