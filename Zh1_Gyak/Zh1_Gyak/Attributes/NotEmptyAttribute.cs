﻿using System;

namespace Zh1_Gyak.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class NotEmptyAttribute : Attribute
    {
    }
}
