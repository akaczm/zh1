﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Zh1_Gyak.Entities;

namespace Zh1_Gyak.Xml
{
    class XmlReader
    {
        public XmlReader(string fileName)
        {
            FileName = fileName;
        }

        public string FileName { get; }

        public List<Person> Read()
        {
            List<Person> list = new List<Person>();
            //string xml = File.ReadAllText(FileName);
            XDocument doc = XDocument.Load(FileName);
            foreach (var xmlPerson in doc.Root.Elements("person"))
            {
                list.Add(new Person
                {
                    Name = xmlPerson.Element("name").Value,
                    Email = xmlPerson.Element("email").Value,
                    Dept = xmlPerson.Element("dept").Value,
                    Rank = xmlPerson.Element("rank").Value,
                    Phone = xmlPerson.Element("phone").Value,
                    Room = GetRoom(xmlPerson.Element("room").Value)
                });
            }

            return list;
        }

        private int GetRoom(string value)
        {
            string[] roomParts = value.Split('.');
            return roomParts.Length > 1 && int.TryParse(roomParts[1], out int result) ? result : -1;
        }
    }
}
