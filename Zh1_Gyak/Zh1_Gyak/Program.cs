﻿using System;
using System.Collections.Generic;
using Zh1_Gyak.Converters;
using Zh1_Gyak.Data;
using Zh1_Gyak.Entities;
using Zh1_Gyak.Validators;
using Zh1_Gyak.Xml;

namespace Zh1_Gyak
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlReader reader = new XmlReader("people.xml");
            List<Person> list = reader.Read();
            foreach (var person in list)
            {
                try
                {
                    Validator.Validate(person);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("{0}: {1}", person.Name, ex.Message);
                }
            }

            using (var context = new PeopleDataContext())
            {
                foreach (var localPerson in list)
                {
                    try
                    {
                        Validator.Validate(localPerson);
                        person ppl = new person();
                        PersonConverter.ConvertProperties(localPerson, ppl);
                        context.people.Add(ppl);
                    }
                    catch
                    {
                    }
                }
                context.SaveChanges();
            }
        }
    }
}
