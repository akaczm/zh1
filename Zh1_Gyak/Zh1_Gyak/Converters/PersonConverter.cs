﻿using System;
using System.Linq;
using System.Reflection;
using Zh1_Gyak.Attributes;

namespace Zh1_Gyak.Converters
{
    public static class PersonConverter
    {
        public static void ConvertProperties(object source, object destination)
        {
            foreach (var srcProp in source.GetType().GetProperties())
            {
                var attr = (DbPropertyNameAttribute)srcProp.GetCustomAttributes(typeof(DbPropertyNameAttribute), true).FirstOrDefault();
                //var attr = prop.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(DbPropertyNameAttribute));
                if (attr != null)
                {
                    var destProp = destination.GetType().GetProperty(attr.ColumnName);
                    if (destProp != null)
                    {
                        destProp.SetValue(destination, srcProp.GetValue(source));
                    }
                }
            }
        }

        //private static PropertyInfo GetDestinationProperty(object destination, string propertyName)
        //{
        //    var prop = destination.GetType().GetProperty(propertyName);
        //}
    }
}
